import { checkNetwork } from '../../utils/network.js'
import { formatTime, formatMonth } from '../../utils/util.js'
import { checkComplete, changeInput, changePicker, formatInfo} from '../../utils/resume.js'
import { phoneReg, emailReg, nameReg } from '../../utils/regex.js';
import { sendSuccess } from '../../utils/error.js'
const app = getApp();
const infoName = ['gender', 'birth_date', 'phone', 'resume_name', 'location', 'email', 'introduction', 'now_status'];

Page({
  data: {
    genderArray: ['男', '女'],
    // yearArray: ['0年', '1年', '2年', '3年', '4年', '5年', '6年', '7年', '8年', '9年', '10年', '10年以上'],
    now_statusArray: ['我目前已离职，可快速到岗', '我目前在职，正在考虑换个新环境', '我暂时不想找工作', '我是应届毕业生', '我是在校大学生'],
  },
  inputChange(e) {
    changeInput(e, 'info', this);
  },
  pickerChange(e) {
    changePicker(e, 'info', this);
  },
  locationChange(e) {
    const location = e.detail.value[0] + ' ' + e.detail.value[1] + ' ' + e.detail.value[2];
    this.setData({
      ["info.location"]: location
    })
    console.log(this.data.info.location)
  },
  saveInfo() {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        let info = this.data.info;
        // 准备：检查用户信息完整性
        info = checkComplete(info, infoName);
        if (!info) return;
        // 准备：检查用户信息正确性
        const testName = nameReg.test(info.resume_name);
        const testPhone = phoneReg.test(info.phone);
        const testEmail = emailReg.test(info.email);
        if (!testName) {
          wx.showModal({
            title: '保存失败',
            content: '请填写正确的真实姓名',
          })
          return;
        }
        if (!testPhone) {
          wx.showModal({
            title: '保存失败',
            content: '请填写正确的手机号码',
          })
          return;
        }
        if (!testEmail) {
          wx.showModal({
            title: '保存失败',
            content: '请填写正确的邮箱',
          })
          return;
        }
        info.birth_date = new Date(info.birth_date);

        wx.request({
          url: this.data.url,
          header: app.globalData.header,
          method: 'POST',
          data: {"resume": info},
          success: res => {
            if (!sendSuccess(res.statusCode)) return;

            // 下面的那部分是用来获取总简历ID，来便于以后进行读写操作，
            // 其实如果后端发数据给我的话那就不需要这步
            if (this.data.url === 'https://www.simianti.top/smtProfile/Resume/insert') {
              wx.request({
                url: 'https://www.simianti.top/smtProfile/Resume/ResumeSum',
                header: app.globalData.header,
                method: 'POST',
                success: res => {
                  if (res.statusCode != 200) {
                    wx.showModal({
                      title: '发送失败',
                      content: '获取简历信息失败，请稍后再试',
                      showCancel: false
                    })
                    return;
                  }
                  console.log(info);
                  info = res.resData.resume;
                }
              })
            }

            // 这个是个来在前面的页面设置的
            info.birth_date = formatMonth(info.birth_date);
            const pages = getCurrentPages();
            const prevPage = pages[pages.length - 2];
            prevPage.data.changeInfo = true,
            prevPage.setData({
              info,
              updateSuccess: true,
              editTime: this.data.editTime,
            }, _ => {
              formatInfo(prevPage)
              wx.navigateBack({
                delta: 1,
              })
            })
          },
          complete(res) {
            console.log(res);
          }
        })
      })
  },
  onLoad(options) {
    // 如果上个页面传来的数据有info，那么要设置一下
    if (Object.keys(options).findIndex(key => key == 'info') != -1) {
      const info = JSON.parse(options.info)
      const locationArray = info.location.split(' ');
      const now_statusIndex = this.data.now_statusArray.findIndex(key => key == info.now_status);
      this.setData({
        info,
        locationArray,
        now_statusIndex,
        hasInfo: true
      })
    }
    const now = new Date();
    // endDate用来限制时间选择时最晚的时间
    const endDate = formatMonth(now);
    const editTime = formatTime(now);
    this.data.editTime = editTime;

    let url = null;
    if (!this.data.hasInfo) {
      url = "https://www.simianti.top/smtProfile/Resume/insert";
    } else {
      url = 'https://www.simianti.top/smtProfile/Resume/update';
    }

    this.setData({
      endDate,
      url
    });
  },
  onShareAppMessage: function () {
    return {
      path: '/pages/index/index',
      title: '向你推荐',
    }
  },
})