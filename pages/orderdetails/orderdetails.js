var formatTime = require('../../utils/util.js').formatTime;
var addHalfHour = require('../../utils/util.js').addHalfHour;

Page({
  data: {
    totalPrice: 0,
    carArray: [],
    cur_time: null,
  },

  onShareAppMessage: res => {
    return {
      title: '分享手气红包',
      path: '/pages/index/index',
      imageUrl: '/smt-image/redbag.jpeg',
    }
  },

  onLoad: function (options) {
    var cur_time = new Date();
    this.setData({
      totalPrice: options.totalPrice,
      cur_time: formatTime(cur_time),
      arrive_time: addHalfHour(cur_time),
      carArray: JSON.parse(options.carArray),
      userName: options.userName,
      detailInfo: options.detailInfo,
      telNumber: options.telNumber,
    })
  },

  contactSeller: function () {
    wx.makePhoneCall({
      phoneNumber: '13697492351'
    })
  },
})