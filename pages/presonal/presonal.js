import { checkNetwork } from '../../utils/network.js'
import { checkLogin, register, logout } from '../../utils/login';
const app = getApp();

Page({
  data: {
  },

  onLoad() {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo
      })
    }
  },
  onShow() {
    checkNetwork()
  },

  // 获取用户信息
  getInfo(userInfoRaw) {
    // const this = this
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        if (userInfoRaw.detail.errMsg == 'getUserInfo:ok') {
          app.globalData.grantInfo = true;
          this.setData({
            loginSpin: true,
          })
          // 如果用户正在登录，那就等待登录完成，否则开始登录
          app.globalData.loginPromise
            .then(userInfo => {
              this.setData({
                userInfo,
                loginSpin: false
              })
            })
            .catch(values => {
              app.globalData.loginPromise = new Promise((resolve, reject) => {
                checkLogin(resolve, reject);
              })
              app.globalData.loginPromise
                .then(userInfo => {
                  if (!this.data.resumeSpin) {
                    wx.showToast({
                      title: '登录成功',
                    })
                  }
                  this.setData({
                    userInfo,
                    loginSpin: false
                  })
                })
            })
        } else {
          console.log('用户不授权')
          wx.showModal({
            title: '提示',
            content: '需要授权才能获取资料',
            showCancel: false
          })
        }
      })
  },

  openResume() {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        if (!this.data.resumeSpin) {
          if (!app.globalData.userInfo) {
            this.setData({
              resumeSpin: true
            })
          }
          app.globalData.loginPromise
            .then(values => {
              this.setData({
                resumeSpin: false
              })
              wx.navigateTo({
                url: `/pages/resume/resume`,
              })
            })
            .catch(_ => {
              wx.showModal({
                title: '打开失败',
                content: '请在登录后再进入简历模块，如已授权，请稍后再试',
                showCancel: false
              })
              this.setData({
                resumeSpin: false
              })
            })
        }
      })
  },

  //低版本微信无法使用getUserInfo的button组件，需做兼容处理
  canILogin(e) {
    if (!wx.canIUse('button.open-type.getUserInfo')) {
      wx.showModal({
        title: "微信版本太旧",
        content: '使用旧版本微信，将无法登录、使用一些功能。请至 App Store、Play Store 或其他可信渠道更新微信。',
        confirmText: '好',
        showCancel: false
      })
    }
  },

  //登出
  drop() {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        app.globalData.loginPromise
          .then(_ => {
            // let logoutSpin = true
            this.setData({
              logoutSpin: true
            })
            const spinPromise = new Promise((resolve, reject) => {
              setTimeout(_ => {
                resolve()
              }, app.globalData.spinTime)
            })
            app.globalData.logoutPromise = new Promise((resolve, reject) => {
              logout(resolve, reject);
            })
              .then(_ => {
                // loggingOut = false;
                wx.clearStorage();
                spinPromise
                  .then(_ => {
                    this.setData({
                      logoutSpin: false
                    })
                  })
                this.setData({
                  userInfo: null
                })
                wx.showToast({
                  title: '退出登录成功',
                })
              })
              .catch(_ => {
                spinPromise
                  .then(_ => {
                    this.setData({
                      logoutSpin: false
                    })
                  })
                wx.showModal({
                  title: '退出失败',
                  content: '你的登录过程可能出错，请稍后再试',
                  showCancel: false
                })
              })
          })
          .catch(_ => {
            wx.showModal({
              title: '退出失败',
              content: '你可能还未登录或正在登录，请登陆后再退出登录',
              showCancel: false
            })
          })
      })
  },

  onShareAppMessage() {
    return {
      path: '/pages/index/index',
      title: '向你推荐',
    }
  },
})