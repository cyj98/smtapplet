import { checkNetwork } from '../../utils/network.js'
import { formatTime, formatMonth, formatGraduate } from '../../utils/util.js'
import { checkComplete, changeInput, changePicker} from '../../utils/resume.js'
import { sendSuccess} from '../../utils/error.js'
const app = getApp()
const eduName = ['graduation_date', 'edu_background', 'major', 'school']

Page({
  data: {
    edu_backgroundArray: ['大专', '本科', '硕士', '博士', '其他'],
  },
  onShow() {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
  },
  inputChange(e) {
    changeInput(e, 'edu', this);
  },
  pickerChange(e) {
    changePicker(e, 'edu', this);
  },
  onLoad: function (options) {
    if (Object.keys(options).findIndex(key => key == 'edu') != -1) {
      const edu = JSON.parse(options.edu)
      const edu_backgroundIndex = this.data.edu_backgroundArray.findIndex(key => key == edu.edu_background);
      this.setData({
        edu,
        edu_backgroundIndex
      })
    }
    const date = new Date();
    let endDate = formatGraduate(date); // 用于获取四年后的时间作为选取的结束时间
    const editTime = formatTime(date);
    this.data.hasEdu = this.data.edu ? true : false;
    let url = null;
    if (!this.data.hasEdu) {
      url = "https://www.simianti.top/smtProfile/ResumeEducation/insert";
    } else {
      url = 'https://www.simianti.top/smtProfile/ResumeEducation/update';
    }
    this.data.editTime = editTime;
    this.setData({
      endDate,
      url
    });
  },
  onShareAppMessage: function () {
    return {
      path: '/pages/index/index',
      title: '向你推荐',
    }
  },
  saveEdu: function (e) {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        let edu = this.data.edu;
        edu = checkComplete(edu, eduName);
        if (!edu) return;

        edu.graduation_date = new Date(edu.graduation_date);
        edu.enrollment_date = new Date();
        
        wx.request({
          url: this.data.url,
          header: app.globalData.header,
          method: 'POST',
          data: {"resumeEducation" : edu},
          success: res => {
            if (!sendSuccess(res.statusCode)) return;
            const pages = getCurrentPages();
            const prevPage = pages[pages.length - 2];
            edu['graduation_date'] = formatMonth(edu.graduation_date);
            prevPage.setData({
              edu,
              updateSuccess: true,
              editTime: this.data.editTime
            }, _ => {
              wx.navigateBack({
                delta: 1,
              })
            })
          },
          complete(res) {
            console.log(res);
          }
        })
      })
  },
})