import { phoneReg, emailReg, nameReg } from '../../utils/regex.js';
import { checkInclude } from '../../utils/resume.js'
const app = getApp();
// const operaName = ['mass', 'trade', 'location', 'contact_name', 'phone',   'email', 'need', ];

Page({
  data: {},
  inputChange(e) {
    const subOpera = "opera." + e.currentTarget.dataset.name
    this.setData({
      [subOpera]: e.detail.value,
    })
  },
  saveOpera() {
    let opera = this.data.opera;
    if (!opera) {
      wx.showModal({
        title: '提交失败',
        content: '请至少填写手机号码和业务需求后进行提交',
        showCancel: false
      })
      return;
    }
    if (!opera.message) {
      wx.showModal({
        title: '提交失败',
        content: '请填写业务需求后在进行提交',
        showCancel: false
      })
      return;
    }
    if (opera.contact_name) {
      const testName = nameReg.test(opera.contact_name);
      if (!testName) {
        wx.showModal({
          title: '提交失败',
          content: '请填写正确的真实姓名',
          showCancel: false
        })
        return;
      }
    }
    if (!opera.phone) {
      wx.showModal({
        title: '提交失败',
        content: '请至少填写手机号码后提交',
        showCancel: false
      })
      return;
    }
    if (opera.phone) {
      const testPhone = phoneReg.test(opera.phone);
      if (!testPhone) {
        wx.showModal({
          title: '提交失败',
          content: '请填写正确的手机号码',
          showCancel: false
        })
        return;
      }
    }
    if (opera.email) {
      const testEmail = emailReg.test(opera.email);
      if (!testEmail) {
        wx.showModal({
          title: '提交失败',
          content: '请填写正确的邮箱',
          showCancel: false
        })
        return;
      }
    }
    wx.request({
      url: 'https://www.simianti.top/smtProfile/business/insert',
      data: JSON.stringify({
        "wechat_id": null,
        "trade": opera.trade,
        "business_content": opera.message,
        "phone": opera.phone,
        "name": opera.name,
        "location": opera.location,
        "unit_or_persion": opera.mess,
        "email": this.data.email,
      }),
      method: 'POST',
      header: app.globalData.header,
      success: res => {
        if (res.statusCode != 200) {
          wx.showModal({
            title: '提交失败',
            content: '提交简历信息失败，服务器可能正在维护，请稍后再试',
            showCancel: false
          })
          return;
        }
        const pages = getCurrentPages();
        const prevPage = pages[pages.length - 2];
        prevPage.setData({
          submitSuccess: true,
        }, _ => {
          wx.navigateBack({
            delta: 1,
          })
        })
      },
      complete: res => {
      }
    })
  },
  onShareAppMessage: function () {
    return {
      path: '/pages/index/index',
      title: '向你推荐',
    }
  }
})
