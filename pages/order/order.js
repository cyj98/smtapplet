var addHalfHour = require('../../utils/util.js').addHalfHour;

Page({
  data: {
    totalPrice: 0,
    carArray: [],
  },

  onLoad: function (options) {
    var arrive_time = addHalfHour(new Date());
    this.setData({
      totalPrice: options.totalPrice,
      carArray: JSON.parse(options.carArray),
      arrive_time: arrive_time,
    })
  },

  onShareAppMessage: function () {
    return {
      path: '/pages/index/index',
      title: '转发给好友',
    }
  },

  openOrderDetails: function () {
    if (this.data.detailInfo) {
      wx.navigateTo({
        url: `/pages/orderdetails/orderdetails?carArray=` + JSON.stringify(this.data.carArray) + '&totalPrice=' + this.data.totalPrice + '&userName=' + this.data.userName + '&detailInfo=' + this.data.detailInfo + '&telNumber=' + this.data.telNumber,
      })
    } else {
      wx.showModal({
        title: '支付失败',
        content: '请输入收货信息后再进行微信支付',
        success: res => {
          if (res.confirm === true){
            this.getAddress();
          }
        }
      })
    }
  },

  getAddress() {
    wx.chooseAddress({
      success: res => {
        console.log(res)
        this.setData({
          userName: res.userName,
          detailInfo: res.provinceName + res.cityName + res.countyName + res.detailInfo,
          telNumber: res.telNumber
        })
      }
    })
  },
})