import {
  checkNetwork
} from '../../utils/network.js'
const app = getApp();

Page({
  data: {
    showSendStatus: false,
    showOverStatus: false,
  },

  onShareAppMessage: function() {
    return {
      path: '/pages/index/index',
      title: '向你推荐',
    }
  },

  lookResume() {
    wx.navigateTo({
      url: '/pages/resume/resume',
    })
  },
  switchSend: function() {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        if (!app.globalData.userInfo) {
          wx.showModal({
            title: '应聘失败',
            content: '你仍未登录，请登录或重新登录后重新。',
          })
          return;
        }
        wx.request({
          url: 'https://www.simianti.top/smtProfile/Resume/ResumeSum',
          header: app.globalData.header,
          method: 'POST',
          success: (res) => {
            if (res.statusCode != 200) {
              wx.showModal({
                title: '投递失败',
                content: '服务器正在繁忙，请稍后再试',
              })
              return;
            }
            console.log('resume', res);
            const resData = res.data.resData;
            if (resData) {
              if (resData.resume && resData.resume_Education && resData.resume_Education.length > 0) {
                this.setData({
                  showSendStatus: !this.data.showSendStatus
                })
              } else {
                wx.showModal({
                  title: '提交失败',
                  content: '请填写个人简历和教育简历后再进行提交',
                })
              }
            } else {
              wx.showModal({
                title: '获取失败',
                content: '从服务器获取简历信息失败，请稍后再试',
                showCancel: false
              })
            }
          },
          fail: res => {
            wx.showModal({
              title: '获取失败',
              content: '从服务器获取简历信息失败，请稍后再试',
              showCancel: false
            })
          }
        })
      })
  },
  stopMove() {},
  insertInterview() {
    const jobDetail = this.data.jobDetail;
    const recruitment_id = jobDetail.recruitment_id;
    const sentInterview = {
      "recruitment_id": recruitment_id,
      // "Resume_resume_id": 
      "interview": {
        "interview_location": "广药",
        "remark": "无",
        "interview_time": new Date(),
        "interview_status": "面试状态"
      }
    }
    wx.request({
      url: 'https://www.simianti.top/smtProfile/Interview/insert',
      header: app.globalData.header,
      method: 'POST',
      data: sentInterview,
      success: (res) => {
        const pages = getCurrentPages();
        const prevPage = pages[pages.length - 2];
        prevPage.setData({
          insertSuccess: true,
        })
      },
      complete(res) {
        console.log('insertInterview success\n', res)
      },
    })
    this.setData({
      showOverStatus: !this.data.showOverStatus
    })
  },
  onLoad(data) {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        this.setData({
          loadSpin: true
        })
        const jobDetail = JSON.parse(data.jobDetail);
        const city = jobDetail.post.split('——')[1]
        const spinPromise = new Promise((resolve, reject) => {
          setTimeout(_ => {
            resolve()
          }, app.globalData.spinTime)
        })
        wx.request({
          url: 'https://www.simianti.top/smtProfile/Interview/IsHas',
          header: app.globalData.header,
          data: {
            "recruitment_id": jobDetail.recruitment_id
          },
          method: 'POST',
          success: res => {
            console.log('load job details\n', res)
            if (res.data.resData == 'has data') {
              this.setData({
                showOverStatus: true
              })
            }
          },
          complete: res => {
            spinPromise.then(_ => {
              this.setData({
                loadSpin: false,
              })
            })
          }
        })
        this.setData({
          jobDetail,
          city
        })
      })
  }
})