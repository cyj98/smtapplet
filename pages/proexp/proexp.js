// pages/proexperience/proexperience.js
import { checkNetwork } from '../../utils/network.js'
import { formatMonth, formatTime } from '../../utils/util.js'
import { checkComplete, changeInput} from '../../utils/resume.js'
import { sendSuccess, dateCompare } from '../../utils/error.js'
const app = getApp();
const proName = ['time_start', 'business_name', 'business_post', 'time_end', 'business_content'];

Page({
  inputChange(e) {
    changeInput(e, 'proExp', this);
  },
  onLoad(options) {
    if (Object.keys(options).findIndex(key => key == 'proExp') != -1) {
      const proExp = JSON.parse(options.proExp)
      this.data.index = options.index;
      this.setData({
        proExp,
        index: options.index
      })
    }
    const date = new Date();
    const endDate = formatMonth(date);
    this.data.editTime = formatTime(date);
    let url = null;
    const operation = options.operation;
    this.data.operation = operation;
    if (operation == 'add') {
      url = "https://www.simianti.top/smtProfile/ResumeBusiness/insert";
    } else {
      url = 'https://www.simianti.top/smtProfile/ResumeBusiness/update';
    }
    this.setData({
      endDate,
      url,
    });
  },
  savePro: function (e) {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        let proExp = this.data.proExp;
        proExp = checkComplete(proExp, proName)
        if (!proExp) return;

        proExp['time_start'] = new Date(proExp.time_start);
        proExp['time_end'] = new Date(proExp.time_end);
        if (!dateCompare(proExp.time_start, proExp.time_end)) return;
          wx.request({
            url: this.data.url,
            header: app.globalData.header,
            method: 'POST',
            data: proExp,
            success: res => {
              if (!sendSuccess(res.statusCode)) return;
              const pages = getCurrentPages();
              const prevPage = pages[pages.length - 2];
              let proExps = prevPage.data.proExp;
              const index = this.data.index;
              proExp['time_start'] = formatMonth(proExp.time_start);
              proExp['time_end'] = formatMonth(proExp.time_end);
              if (this.data.operation == 'edit') {
                proExps[index] = proExp;
              } else {
                proExp.resume_business_id = res.data.resData;
                proExps.push(proExp);
              }
              prevPage.setData({
                proExp: proExps,
                updateSuccess: true,
                editTime: this.data.editTime
              }, _ => {
                wx.navigateBack({
                  delta: 1,
                })
              })
            },
            complete(res) {
              console.log(res);
            }
          })
        })
  },
  deletePro: function () {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        wx.showModal({
          content: '确认删除？',
          success: res => {
            if (res.confirm == true) {
              wx.request({
                url: 'https://www.simianti.top/smtProfile/ResumeBusiness/delete',
                header: app.globalData.header,
                data: {
                  "resume_business_id": this.data.proExp.resume_business_id
                },
                method: 'POST',
                success: res => {
                  if (!sendSuccess(res.statusCode)) return;
                  const pages = getCurrentPages();
                  const prevPage = pages[pages.length - 2];
                  let proExp = prevPage.data.proExp;
                  proExp.splice(this.data.index, 1);
                  prevPage.setData({
                    proExp,
                    deleteSuccess: true,
                    editTime: this.data.editTime,
                  }, _ => {
                    wx.navigateBack({
                      delta: 1,
                    })
                  })
                },
                complete: res => {
                  console.log(res);
                }
              })
            }
          }
        })
      })
  },
  onShareAppMessage: function () {
    return {
      path: '/pages/index/index',
      title: '向你推荐',
    }
  },
})