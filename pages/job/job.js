import {
  checkNetwork
} from '../../utils/network.js'
const app = getApp()
Page({
  openJob: function(e) {
    const num = e.currentTarget.dataset.num;
    const jobDetail = JSON.stringify(this.data.jobDetails[num]);
    wx.navigateTo({
      url: `/pages/jobdetails/jobdetails?jobDetail=${jobDetail}`,
    })
  },
  onLoad() {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        this.setData({
          loadSpin: true
        })
        const spinPromise = new Promise((resolve, reject) => {
          setTimeout(_ => {
            resolve()
          }, app.globalData.spinTime)
        })
        wx.request({
          url: 'https://www.simianti.top/smtProfile/Recruitment/list',
          header: app.globalData.header,
          method: 'POST',
          success: (res) => {
            if (res.statusCode != 200) {
              wx.showModal({
                title: '获取简历列表失败',
                content: '服务器可能正在维护，请稍后再试',
                showCancel: false
              })
              return;
            }
            const jobDetails = res.data.resData;
            this.setData({
              jobDetails
            })
          },
          complete: res => {
            console.log('getJobDetails', res)
            spinPromise.then(_ => {
              this.setData({
                loadSpin: false,
              })
            })
          },
        })
      })
  },
  onShareAppMessage: function() {
    return {
      path: '/pages/index/index',
      title: '向你推荐',
    }
  },
  onShow: function() {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        if (this.data.insertSuccess) {
          wx.showToast({
            title: '提交成功',
          })
          this.data.insertSuccess = false
        }
      })
  }
})