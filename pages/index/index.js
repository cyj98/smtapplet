const app = getApp()
import { checkLogin } from '../../utils/login.js';
import { getImageUrl, getImage } from '../../utils/url.js';
import { phoneReg } from '../../utils/regex.js'
import { checkNetwork } from '../../utils/network.js'
// import phoneReg from '../../utils/regex.js';
// const phoneReg = require('../../utils/regex.js');
// const ImgLoader = require('../../img-loader/img-loader.js')
// this.imgLoader = new ImgLoader(this)
// this.imgLoader.load(imgUrlOriginal, (err, data) => {
//   console.log('图片加载完成', err, data.src, data.width, data.height)
// })


Page({
  data: {
    imgUrls: [
      "https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8ep-FECH6fdk_UM0Lglx_B9qGZZ_0zVtS5iCkhaYeYO6w%3D%3D.jpg?Expires=253363115271&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=Z0%2FBklvHsi884wnbON17bivQncM%3D",
      "https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8dE7hLaThnobcAoZX5RInkoowbPBmxzNn_KOBJxx9pT-g%3D%3D.jpg?Expires=253363115270&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=uHP94zbFnGzhqwv9bMFHAl1AyhY%3D",
      "https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8fbrfB8HINYu9fQVz1vy5-axw2dPi92vDAGk8unLps_Fg%3D%3D.jpg?Expires=253363115271&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=DkDtuvVdn6hHowfuho62KDDXTyM%3D",
      "https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8eK1D5-OYX69zDVEUwptDbWkp6PBqQskhq_FcPIR1IPyQ%3D%3D.jpg?Expires=253363115269&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=jG1WthZ8163OteVZ4C%2BmoQ%2B9cHE%3D",
      "https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8fhpIz-BdnXYKIjQ3J_LMtuOA30hlmSQ7pKNdG381OKlQ%3D%3D.jpg?Expires=253363115270&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=0N5lssLDQNfvd32E%2BEner%2BTNq90%3D",
      "https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8c-xc9sS5FUkHs3ME9o8_t6GfbrllExzXx9Om54szPARg%3D%3D.jpg?Expires=253363115271&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=xbfjfdHZEKatRnWsxo4f%2FYlm96A%3D",
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 2000,
    cirCular: true,
    // duration: 1000,
    // refreshTime: '',
    // animationData: {},
    firstEnter: true
  },
  onShow: function () {
      checkNetwork()
    if (this.data.submitSuccess) {
      wx.showToast({
        title: '提交成功',
      })
      this.data.submitSuccess = false
    }
  },
  onReady() {
    // networkPromise是用来确保有网络再进行下一步操作的
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        // 用loginPromise是为了在其他操作里面确保已经登录（解决异步问题）
        if (app.globalData.userInfo) {
          app.globalData.loginPromise = new Promise((resolve, reject) => {
            checkLogin(resolve, reject);
          })
        } else {
          app.globalData.loginPromise = Promise.reject()
        }
      })
  },
  onShareAppMessage: function (res) {
    return {
      title: '向你推荐',
    }
  },
    // 名字懒得改，其实这个是用来打开业务咨询页面的
  showCustomDialog() {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        if (!app.globalData.userInfo) {
          wx.showModal({
            title: '打开失败',
            content: '请登录后再进行业务咨询',
          })
          return;
        }
        wx.navigateTo({
          url: '/pages/opera/opera',
        })
      })
    // this.setData({ show: true });
  },

  stopMove() { },

  // onClose(event) {
  //   if (event.detail === 'confirm') {
  //     setTimeout(() => {
  //       this.setData({
  //         show: false
  //       });
  //     }, 1000);
  //   } else {
  //     this.setData({
  //       show: false
  //     });
  //   }
  // },

  // switchModal: function () {
  //   if (!app.globalData.userInfo) {
  //     wx.showModal({
  //       title: '填写失败',
  //       content: '你可能尚未登录或登录态过期，请重新登录后再尝试',
  //     })
  //   } else {
  //     this.setData({
  //       showModalStatus: !this.data.showModalStatus,
  //     })
  //   }
  // },

  // getName(e) {
  //   this.data.name = e.detail;
  // },
  // getTrade(e) {
  //   this.data.trade = e.detail;
  // },
  // getPhone(e) {
  //   this.data.phone = e.detail;
  // },
  // getLocation(e) {
  //   this.data.location = e.detail;
  // },
  // getMail(e) {
  //   this.data.mail = e.detail;
  // },
  // getMessage(e) {
  //   this.data.message = e.detail
  // },
  // getUserInfo(event) {
  //   console.log(event.detail);
  // },



  // formSubmit: function (e) {
  // const checkPhone = phoneReg.test(value.phone);
  // if (!checkPhone) {
  //   wx.showModal({
  //     title: '提交失败',
  //     content: '请填写真实的手机号码',
  //     showCancel: false
  //   })
  //   return;
  // }
  // wx.request({
  //   url: 'https://www.simianti.top/smtProfile/business/insert',
  //   data: JSON.stringify({
  //     "wechat_id": null,
  //     "trade": this.data.trade,
  //     "business_content": this.data.message,
  //     "phone": this.data.phone,
  //     "name": this.data.name,
  //     "location": this.data.location,
  //     "unit_or_persion": 0,
  //     "email": this.data.email,
  //   }),
  //   method: 'POST',
  //   header: app.globalData.header,
  //   success: res => {
  //     if (res.statusCode != 200) {
  //       wx.showModal({
  //         title: '提交失败',
  //         content: '提交简历信息失败，服务器可能正在维护，请稍后再试',
  //         showCancel: false
  //       })
  //       return;
  //     }
  //     wx.showToast({
  //       title: '提交成功',
  //     })
  //     this.setData({
  //       showModalStatus: false
  //     })
  //   },
  //   complete: res => { 
  //   }
  // })
  // },
  // onLoad: _ => {
  // this.imgLoader = new ImgLoader(this);
  // },
}) 