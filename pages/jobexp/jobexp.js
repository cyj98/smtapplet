import { checkNetwork } from '../../utils/network.js'
import { formatMonth, formatTime } from '../../utils/util.js'
import { checkComplete, changeInput} from '../../utils/resume.js'
import { sendSuccess, dateCompare } from '../../utils/error.js'
const app = getApp()
const jobName = ['time_start', 'company_name', 'job_post', 'time_end', 'job_content'];

Page({
  onShareAppMessage() {
    return {
      path: '/pages/index/index',
      title: '向你推荐',
    }
  },
  inputChange(e) {
    changeInput(e, 'jobExp', this);
  },
  saveJob() {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        let jobExp = this.data.jobExp;
        jobExp = checkComplete(jobExp, jobName)
        if (!jobExp) return;
        console.log('After delete\n',jobExp)

        //  把时间转化为服务器可以识别的时间
        jobExp['time_start'] = new Date(jobExp.time_start);
        jobExp['time_end'] = new Date(jobExp.time_end);
        if (!dateCompare(jobExp.time_start, jobExp.time_end)) return;
        wx.request({
          url: this.data.url,
          header: app.globalData.header,
          method: 'POST',
          data: jobExp,
          success: res => {
            if (!sendSuccess(res.statusCode)) return;
            // 格式化时间为便于人类阅读的时间
            jobExp['time_start'] = formatMonth(jobExp.time_start);
            jobExp['time_end'] = formatMonth(jobExp.time_end);
            const pages = getCurrentPages();
            const prevPage = pages[pages.length - 2];
            let jobExps = prevPage.data.jobExp;
            const index = this.data.index;
            if (this.data.operation == 'edit') {
              jobExps[index] = jobExp;
            } else {
              jobExp.idResume_Company = res.data.resData;
              jobExps.push(jobExp);
            }
            prevPage.setData({
              jobExp: jobExps,
              updateSuccess: true,
              editTime: this.data.editTime
            }, _ => {
              wx.navigateBack({
                delta: 1,
              })
            })
          },
          complete(res) {
            console.log(res);
          }
        })
      })
  },

  deleteJob: function () {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        wx.showModal({
          content: '确认删除？',
          success: res => {
            if (res.confirm == true) {
              wx.request({
                url: 'https://www.simianti.top/smtProfile/ResumeCompany/delete',
                header: app.globalData.header,
                data: {
                  "idResume_Company": this.data.jobExp.idResume_Company
                },
                method: 'POST',
                success: res => {
                  if (!sendSuccess(res.statusCode)) return;
                  const pages = getCurrentPages();
                  const prevPage = pages[pages.length - 2];
                  let jobExp = prevPage.data.jobExp;
                  jobExp.splice(this.data.index, 1);
                  prevPage.setData({
                    jobExp,
                    deleteSuccess: true,
                    editTime: this.data.editTime,
                  }, _ => {
                    wx.navigateBack({
                      delta: 1,
                    })
                  })
                },
                complete: res => {
                  console.log(res);
                }
              })
            }
          }
        })
      })
  },
  onLoad: function (options) {
    if (Object.keys(options).findIndex(key => key == 'jobExp') != -1) {
      const jobExp = JSON.parse(options.jobExp)
      this.setData({
        jobExp,
        index: options.index
      })
    }
    const date = new Date();
    const endDate = formatMonth(date);
    this.data.editTime = formatTime(date);
    let url = null;
    const operation = options.operation;
    this.data.operation = operation;
    if (options.operation == 'add') {
      url = "https://www.simianti.top/smtProfile/ResumeCompany/insert";
    } else {
      url = 'https://www.simianti.top/smtProfile/ResumeCompany/update';
    }
    this.setData({
      endDate,
      url
    });
  },

})