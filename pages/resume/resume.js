import {
  checkNetwork
} from '../../utils/network.js'
import {
  formatMonth
} from '../../utils/util.js'
import {
  formatInfo
} from '../../utils/resume.js'
// import { uploadImageUrl, deleteImageUrl } from '../../utils/url.js';
const app = getApp()

Page({
  data: {
    jobExps: [],
    proExp: []
  },
  editInfo: function() {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        const page = `/pages/info/info` + (this.data.info ? `?info=${JSON.stringify(this.data.info)}` : '')
        wx.navigateTo({
          url: page
        })
      })
  },
  editEdu: function() {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        if (!this.data.info) {
          wx.showModal({
            title: '添加失败',
            content: '请编辑个人信息后再添加其他简历',
            showCancel: false
          })
        } else {
          const page = `/pages/edu/edu` + (this.data.edu ? `?edu=${JSON.stringify(this.data.edu)}` : '');
          wx.navigateTo({
            url: page,
          })
        }
      })
  },
  addJobExp: function(e) {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        if (!this.data.info) {
          wx.showModal({
            title: '添加失败',
            content: '请编辑个人信息后再添加其他简历',
            showCancel: false
          })
        } else {
          const index = e.currentTarget.dataset.index;
          const page = `/pages/jobexp/jobexp?operation=add`;
          wx.navigateTo({
            url: page
          })
        }
      })
  },
  editJobExp: function(e) {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        const index = e.currentTarget.dataset.index;
        const page = `/pages/jobexp/jobexp?jobExp=${JSON.stringify(this.data.jobExp[index])}&index=${index}&operation=edit`;
        wx.navigateTo({
          url: page
        })
      })
  },
  addProExp: function(e) {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        if (!this.data.info) {
          wx.showModal({
            title: '添加失败',
            content: '请编辑个人信息后再添加其他简历',
            showCancel: false
          })
        } else {
          const index = e.currentTarget.dataset.index;
          const page = `/pages/proexp/proexp?operation=add`;
          wx.navigateTo({
            url: page,
          })
        }
      })
  },
  editProExp: function(e) {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        const index = e.currentTarget.dataset.index;
        const page = `/pages/proexp/proexp?proExp=${JSON.stringify(this.data.proExp[index])}&index=${index}&operation=edit`;
        wx.navigateTo({
          url: page,
        })
      })
  },
  uploadImage() {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        if (!this.data.info) {
          wx.showModal({
            title: '上传失败',
            content: '请填写总简历后再上传图片',
            showCancel: false
          })
        } else {
          wx.chooseImage({
            count: 1,
            success: res => {
              console.log(res);
              if (res.tempFiles[0].size >= 1000000) {
                wx.showModal({
                  title: '上传失败',
                  content: '图片大小超过1M，请上传小于1M的图片',
                })
                return;
              }
              const filePath = res.tempFilePaths[0];
              const name = filePath.slice(filePath.length - 10);
              console.log(filePath);
              console.log(name);
              wx.uploadFile({
                url: "https://www.simianti.top/smtProfile/Resume/insertImage",
                filePath: filePath,
                name: name,
                header: app.globalData.header,
                formData: {
                  imageName: name,
                  imageFrom: "smt",
                  imageType: "wxImage",
                  imageSecure: "1",
                  imagePurpose: "userInfo",
                },
                fail: res => {
                  wx.showModal({
                    title: '上传失败',
                    content: '上传图片失败，请稍后再试',
                    showCancel:false
                  })
                },
                success: res => {
                  const avatarUrl = JSON.parse(res.data).resData;
                  this.setData({
                    avatarUrl
                  })
                }
              })
            }
          })
        }
      })
  },

  onLoad() {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        wx.request({
          url: 'https://www.simianti.top/smtProfile/Resume/ResumeSum',
          header: app.globalData.header,
          method: 'POST',
          success: res => {
            if (res.statusCode != 200) {
              wx.showModal({
                title: '获取信息失败',
                content: '服务器出错，请稍后再试',
                showCancel: false
              })
              return;
            }
            const avatarUrl = res.data.resData.imageUrl;
            this.setData({
              avatarUrl
            })
          }
        })
        const value = wx.getStorageSync('resume')
        if (value) {
          // console.log(res);
          const resume = JSON.parse(value);
          this.setData({
            info: resume.info,
            edu: resume.edu,
            jobExp: resume.jobExp,
            proExp: resume.proExp
          }, _ => {
            this.setData({
              showContent: true
            })
          })
          formatInfo(this)
        } else {
          wx.request({
            url: 'https://www.simianti.top/smtProfile/Resume/ResumeSum',
            header: app.globalData.header,
            method: 'POST',
            success: res => {
              if (res.statusCode != 200) {
                wx.showModal({
                  title: '获取信息失败',
                  content: '正在重新获取信息',
                  showCancel: false
                })
                return;
              }
              console.log(res);
              const resData = res.data.resData;
              if (resData) {
                // 从服务器获取数据
                const info = resData.resume;
                let edu = resData.resume_Education;
                const jobExp = resData.resume_Company;
                const proExp = resData.resume_Business;
                const avatarUrl = resData.imageUrl;
                // 把从服务器获得的数据转化为用户看到的数据
                if (info) {
                  info.birth_date = formatMonth(new Date(info.birth_date))
                }
                if (edu) {
                  edu = edu[0]
                  edu.graduation_date = formatMonth(new Date(edu.graduation_date));
                }
                if (jobExp) {
                  for (let i = 0; i < jobExp.length; ++i) {
                    jobExp[i].time_start = formatMonth(jobExp[i].time_start);
                    jobExp[i].time_end = formatMonth(jobExp[i].time_end);
                  }
                }
                if (proExp) {
                  for (let i = 0; i < proExp.length; ++i) {
                    proExp[i].time_start = formatMonth(proExp[i].time_start);
                    proExp[i].time_end = formatMonth(proExp[i].time_end);
                  }
                }
                this.setData({
                  info: info ? info : null,
                  edu: edu ? edu : null,
                  jobExp: jobExp ? jobExp : null,
                  proExp: proExp ? proExp : null,
                  avatarUrl: avatarUrl ? avatarUrl : null
                }, _ => {
                  if (info) {
                    formatInfo(this)
                  }
                  this.setData({
                    loadSpin: false
                  })
                })
                wx.setStorage({
                  key: 'resume',
                  data: JSON.stringify({
                    info: info,
                    edu: edu,
                    jobExp: jobExp,
                    proExp: proExp
                  }),
                })
              }
            },
            fail: res => {
              this.setData({
                loadSpin: false
              })
              wx.showModal({
                title: '打开失败',
                content: '进入简历页面失败，请稍后再试',
                showCancel: false
              })
            }
          })
        }
      })
  },

  onShow: function() {
    const networkPromise = new Promise((resolve, reject) => {
      checkNetwork(resolve, reject)
    })
    networkPromise
      .then(_ => {
        if (this.data.deleteSuccess || this.data.updateSuccess) {
          wx.showToast({
            // 反正删除或更新两者只能有一个，所以这样写了
            title: this.data.deleteSuccess ? '删除成功' : '更新成功',
          })
          wx.setStorage({
            key: 'resume',
            data: JSON.stringify({
              info: this.data.info,
              edu: this.data.edu,
              jobExp: this.data.jobExp,
              proExp: this.data.proExp
            }),
          })
          this.data.updateSuccess = false;
          this.data.deleteSuccess = false;
        }
      })
  },
  onShareAppMessage: function() {
    return {
      path: '/pages/index/index',
      title: '向你推荐',
    }
  },
})