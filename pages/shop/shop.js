Page({
  data: {
    /* category: [
       { name: '今日推荐', id: 'recommend' },
       { name: '限时促销', id: 'sale' },
       { name: '水/饮料', id: 'drink' },
       { name: '啤酒/鸡尾酒', id: 'liqueur' }
     ],*/
    totalNumber: 0,
    // counter: 0,
    totalPrice: 0,
    curIndex: 0,
    childIndex: 0,
    list_opened: false,
    showModalStatus: false,
    toView: 'recommend',
    carArray: [],

    // pageid: [
    //   { id: 'recommend-page' },
    //   { id: 'sale-page' },
    //   { id: 'drink-page' }
    // ],
    list: [
      {
        id: 'view',
        name: '今日推荐',
        open: false,
        pages: false
      }, {
        id: 'content',
        name: '限时促销',
        open: false,
        pages: false
      }, {
        id: 'form',
        name: '水/饮料',
        open: false,
        pages: ['饮用水', '碳酸饮料', '咖啡茶饮']
      }, {
        id: 'nav',
        name: '酒/鸡尾酒',
        open: false,
        pages: ['啤酒', '鸡尾酒']
      }, {
        id: 'map',
        name: '休闲零食',
        open: false,
        pages: ['薯片膨化', '饼干糕点', '花生瓜子']
      }, {
        id: 'canvas',
        name: '日用百货',
        open: false,
        pages: false
      }
    ],

    recommendList: [
      {
        id: 'recommendList0',
        name: '卫龙大面筋',
        standard: '106g/袋',
        productPrice: '4.5',
        originalPrice: '5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8ftJCYFo6glGwxwYBr0nras98FEXoIrTdt65x6ByvGbhw%3D%3D.jpg?Expires=253362844417&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=%2FBt6kOzF%2BT5RbvGyx41yp75nszQ%3D',
      },
      {
        id: 'recommendList1',
        name: '伊利巧乐兹巧脆棒',
        standard: '175g/袋',
        productPrice: '3.5',
        originalPrice: '5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8f85GfOYFMjP7T5KIEDMKc-ZJTr1lL2qJTF7j3cTyxC8A%3D%3D.jpg?Expires=253362844417&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=1tpVYAq45wp5XNk1y%2B0Q%2Fe%2B6jY4%3D',
      }
    ],

    saleList: [
      {
        id: 'saleList0',
        name: '维他柠檬茶',
        standard: '250ml/盒',
        productPrice: '1.0',
        originalPrice: '3.5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8e3hMZoQu3A96xXW0yJ19AOP-gMJtEbrrGRWrsohfIPkA%3D%3D.jpg?Expires=253362926077&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=PzvkEtsSffY%2B1N95c%2Fb37hWyx%2FI%3D',
      },

      {
        id: 'saleList1',
        name: '维他奶（原味）',
        standard: '250ml/盒',
        productPrice: '1.0',
        originalPrice: '3.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8ckzUBFc8lTrBs5vXptIanYCELO8rTDqseNsLwTDqN_jQ%3D%3D.jpg?Expires=253362844828&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=m%2ByvRD6K6yNBiyQVMep%2FWX7PjEk%3D',
      },

      {
        id: 'saleList2',
        name: '蜜桃乌龙茶',
        standard: '600ml/盒',
        productPrice: '4.5',
        originalPrice: '5.5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8dXqnIKE_vGxaCnnt0ht6hHbqASQVZcSAXZB4YMai7rRA%3D%3D.jpg?Expires=253362844828&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=LFeOIiy9IEP8cSA2xWWM0LCmPBk%3D',
      },

      {
        id: 'saleList3',
        name: '淳风派山椒凤爪',
        standard: '35g/包',
        productPrice: '1.0',
        originalPrice: '2.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8fUB9eoEo8oDlnZICvo1iAixE1k_X845-TTAX2H13k85g%3D%3D.jpg?Expires=253362926130&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=ZP9jBewu%2BDbdTIJblfV2fF3bY6c%3D',
      },
    ],
    waterList: [
      {
        id: 'waterList0',
        name: '怡宝纯净水',
        standard: '550ml/支',
        productPrice: '2.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8fGy2EXl-K6IQz7qyRsMYWq5rf48wIKeMHa7F1awYWFCw%3D%3D.jpg?Expires=253363016170&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=wrhxCBRFimRnR5bfxsL40y%2FOG8Q%3D',
      },

      {
        id: 'waterList1',
        name: '农夫山泉天然矿泉水',
        standard: '555ml/支',
        productPrice: '3.5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8d19NEqBk80Nb6UMXKQuYp7RFkkVth6AuwJy-K6o-NSDA%3D%3D.jpg?Expires=253362844914&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=T%2Bo16lsbw3%2FcMEst8zduNof%2BhmI%3D',
      },

      {
        id: 'waterList2',
        name: '景田百岁山矿泉水',
        standard: '570ml/支',
        productPrice: '3.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8enlMX3RarHvb-TSn3EvjzAgvRqR_bNa0mOFdT0YQ6q6w%3D%3D.jpg?Expires=253362926342&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=XePSVW3JJbXEmcqgkJ4nJok2JDM%3D',
      },
    ],

    sodasList: [
      {
        id: 'sodas0',
        name: '七喜',
        standard: '600ml/瓶',
        productPrice: '3.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8fGSz_dO4dabZADhXmaazE_g1ZygpmTGjtGlfT_x3QwzQ%3D%3D.jpg?Expires=253362844957&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=f7ZBksOsbgLpzJDApQrIxAJlbGo%3D',
      },

      {
        id: 'sodas1',
        name: '百事可乐',
        standard: '2L/瓶',
        productPrice: '7.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8dhIrejxDeRz1g1A1-kE6fXk0UX2n_06Hpn2OX3c9Am6Q%3D%3D.jpg?Expires=253362844957&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=4cQOB%2FwO3c6RvQNhxM5QvmkFZPI%3D',
      },

      {
        id: 'sodas2',
        name: '可口可乐',
        standard: '550ml/瓶',
        productPrice: '3.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8c4XjSfY8OxFHFjsuf64iMe1X_GkdhCNYh6xhttwfC4XQ%3D%3D.jpg?Expires=253362844958&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=ZDDdPynnq12Mn1j67TMK5Kku2EU%3D',
      },

      {
        id: 'sodas3',
        name: '怡泉+C',
        standard: '500ml/瓶',
        productPrice: '4.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8ddecrOnCAhT8GW-rZUui7c5Dtaa_IG-xsZ17nlkRakGw%3D%3D.jpg?Expires=253362844957&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=WK4f8gCFePx5J6%2FZR%2Bc7E2qjd2g%3D',
      },
    ],

    teaList: [
      // {
      //   id: 'tea0',
      //   name: '维他柠檬茶',
      //   standard: '250ml/盒',
      //   productPrice: '3.0',
      //   amount: 0,
      //   src: '../../commodity-image/001.jpg',
      // },

      {
        id: 'tea1',
        name: '统一小茗同学青柠红茶',
        standard: '480ml/瓶',
        productPrice: '5.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8dDrevTtfSL78bV3-QdC3ljfhCjh5wGH5k__oG_c7NOlg%3D%3D.jpg?Expires=253362845051&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=Qw%2B7avjtW%2FVMfyaMRT2Vmueg9Cs%3D',
      },

      {
        id: 'tea2',
        name: '贝纳颂咖啡拿铁',
        standard: '350/瓶',
        productPrice: '6.5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8crVtGbfzsBTuCwiahZmTtJI1MNDGUHoEvquhRnCOgc6g%3D%3D.jpg?Expires=253362845052&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=NChFBaxmx3IOPkshl2R8S58ESM8%3D',
      },

      {
        id: 'tea3',
        name: '雀巢丝滑摩卡咖啡',
        standard: '260ml/瓶',
        productPrice: '6.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8cXkYOQcL74vf1Tk0npXpr-6qKd1oFQ42wVqEMcurgoVw%3D%3D.jpg?Expires=253362845051&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=LNNG0W85H9foO%2BjBbXCxLCnRUfA%3D',
      },

      {
        id: 'tea4',
        name: '香飘飘Meco原味牛乳茶',
        standard: '350ml/瓶',
        productPrice: '10.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8cHhDaydvvoJwB4rX2ro7SbCmeNdXxVsZ13I8-bTRpTPg%3D%3D.jpg?Expires=253362845051&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=gyRlTYBJ7eWOTl5jtPdMt6gYNK0%3D',
      },
    ],

    beerList: [
      {
        id: 'beer0',
        name: '青岛纯生啤酒',
        standard: '500ml/罐',
        productPrice: '5.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8dOAV1ZS-8eX_OkBtOVtYl_3tL1-Kv2JM8i-0WHixgRpA%3D%3D.jpg?Expires=253362926663&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=sg7qBdexSsTEfVBzF1iOTHiZ8ZQ%3D',
      },

      {
        id: 'beer1',
        name: '蓝带啤酒',
        standard: '250ml/罐',
        productPrice: '5.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8cAZn5Xfin07cOcUyd4zSsz4HdmIXhVazsx5KWGQhRbOg%3D%3D.jpg?Expires=253362926663&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=tnAGKBvVTAK5IbqUbqeKcDawtKM%3D',
      },

      {
        id: 'beer2',
        name: '百威啤酒（高罐装）',
        standard: '500/罐',
        productPrice: '8.5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8c8mzPjRHTTvP_gr6PsWKkufD8BBPP2GGmoZ9KxzF8yEA%3D%3D.jpg?Expires=253362926663&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=b5X56q1abYFUAI7T4iuAvFxcce0%3D',
      },

      {
        id: 'beer3',
        name: '哈尔滨小麦王啤酒',
        standard: '330ml/罐',
        productPrice: '4.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8fHvoC-0jSeK1GF_R4HPq7jCo2ApjqXtpKnHwF3qnJHeA%3D%3D.jpg?Expires=253363018104&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=3TwbEzeiEFe9eqorxfsFulXhKVI%3D',
      },

      {
        id: 'beer4',
        name: '珠江0度精品啤酒',
        standard: '600ml/罐',
        productPrice: '4.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8fXtEw9_KG4QQw7lUdFB_S3WZpbLZWFzzUBP0hd8PLhTg%3D%3D.jpg?Expires=253362926664&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=7BQQlGKJq%2FX0Q6SbOSPbBjspPMg%3D',
      },
    ],

    cockTailList: [
      {
        id: 'cock0',
        name: '锐澳白桃味白兰地鸡尾酒',
        standard: '330ml/罐',
        productPrice: '10.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8fQgMo8gOhkMnK8xCDbHbTomqub3Z5XiknMJTaXasotmw%3D%3D.jpg?Expires=253362926934&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=CP%2BVXI2HAm%2FJuJX2PhGsGUzdqSM%3D',
      },

      {
        id: 'cock1',
        name: '锐澳紫葡萄味白兰地鸡尾酒',
        standard: '330ml/罐',
        productPrice: '10.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8f4bfAx2mVtM2Pwm5FSe9Cxwh76iuhyYAeiXp6K_Vc19Q%3D%3D.jpg?Expires=253362926934&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=UfTWAv66YckcFe%2FzUljF0%2F4%2FCH8%3D',
      },

      {
        id: 'cock2',
        name: '锐澳蜜桃味白兰地鸡尾酒',
        standard: '275ml/瓶',
        productPrice: '10.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8djqOHS_Dk7gIWOTTivUpbD8Vbvjv7dzGJeYq13RPz15g%3D%3D.jpg?Expires=253363016170&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=IrMnHHtHKD3IMwaGELaGUr2YT7o%3D',
      },
    ],

    chipsList: [
      {
        id: 'chips0',
        name: '乐事薯片（黄瓜味）',
        standard: '70g/包',
        productPrice: '5.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8e6x166437Pp51ny13hLlJ_hew1UGIgF19N3E7FJ9e9Aw%3D%3D.jpg?Expires=253362927322&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=js31JUiuhPohhWncRHJHEt0nbSQ%3D',
      },

      {
        id: 'chips1',
        name: '乐事薯片（青柠味）',
        standard: '70g/包',
        productPrice: '5.5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8dGXFlvOlbPbfh53uwYNJPGOJpVWwRF6HHAiPAoaWoYQQ%3D%3D.jpg?Expires=253362927323&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=Tq3Lyv20KdEZ4yciFMmt%2FUwIupw%3D',
      },

      {
        id: 'chips2',
        name: '乐事大波浪烤鸡翅味',
        standard: '100g/包',
        productPrice: '8.5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8ewVPpOyl7vsgo0NpUPm_7uGg39ceGdSyKnlYggiCZKbA%3D%3D.jpg?Expires=253362927322&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=%2FBrUXPiOKRvcc63xtolm7ZoLZho%3D',
      },

      {
        id: 'chips3',
        name: '四洲栗一烧（蒲烧鳗鱼味）',
        standard: '150g/桶',
        productPrice: '10.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8c4qrR7AFd5MUahdX6H8sOpBO92I6WBMv76MCIZUOliag%3D%3D.jpg?Expires=253362927323&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=cW9m7B9fIi5Z5VlV6AFKv8nrfEM%3D',
      },
    ],

    cakeList: [
      {
        id: 'cake0',
        name: '好多鱼鲜香海苔味',
        standard: '33g/盒',
        productPrice: '5.5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8e4JXdnGwdZ5xwjk0Q3G-uYV320moH5TSjL56CVIkJTaQ%3D%3D.jpg?Expires=253362927465&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=zlRGZSmKekrVrEZvq4jfOhj%2FGMg%3D',
      },

      {
        id: 'cake1',
        name: '好丽友蛋黄派',
        standard: '276g/盒',
        productPrice: '12.5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8di4B5HZ7oXYlDpcgfH6reOHWmzD-s1lwH41V5MSUBzYw%3D%3D.jpg?Expires=253362927465&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=zS4zvt9mI9poRAvdszYPA8%2B46GA%3D',
      },

      {
        id: 'cake2',
        name: '奥利奥巧克力味夹心饼干',
        standard: '116g/盒',
        productPrice: '8.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8cgx8Qz7ZPPT4UhMFjea8_zYWjt_-yNbUFePSkHuObV1A%3D%3D.jpg?Expires=253362927464&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=JoC7M0g1OvpjUCaTyYCP6WEtXSQ%3D',
      },

      {
        id: 'cake3',
        name: '格力高百奇巧克力味',
        standard: '50g/盒',
        productPrice: '10.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8cB0K_ScyXUQ4akQmkpNKYnfiGJyq4yY-WtqVbQHhE2bA%3D%3D.jpg?Expires=253362927464&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=Ku%2FMU1VCJ%2BwFuWZiGXT7GlgeD0E%3D',
      },

      {
        id: 'cake4',
        name: '法丽兹原味曲奇',
        standard: '102g/盒',
        productPrice: '5.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8c9jZmGq_N0EJmvbAMmMJ_wEZzvk3gHS-z9BHCOJdtvSg%3D%3D.jpg?Expires=253362927569&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=PA%2FsTnQDcXc0TDjHkJ750uXZ8ok%3D',
      },
    ],

    snackList: [
      {
        id: 'snack0',
        name: '淳风派盐焗花生米',
        standard: '120g/包',
        productPrice: '4.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8doYqwPujY9xktBvCDAGgFBTf4GFt-2RGyY2qACPT9-Vg%3D%3D.jpg?Expires=253362927571&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=Cl58yWUNNeAuWfbhc0oyYgcLicQ%3D',
      },

      {
        id: 'snack1',
        name: '淳风派蒜香花生',
        standard: '240g/盒',
        productPrice: '8.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8ciZUOPVxmhWb0yUM0cnIDIWHxdaWo8wASAThJuRqecBA%3D%3D.jpg?Expires=253362927570&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=tgs7nq%2B1oXI5wijnC9bGvBc0iUQ%3D',
      },

      {
        id: 'snack2',
        name: '恰恰山核桃味瓜子',
        standard: '108g/包',
        productPrice: '7.5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8cnhYwXWUHl2GsjqmPzDpvkColh-1hxRZ5Y8tWAaO6wKw%3D%3D.jpg?Expires=253362927570&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=G6ZYbX27x5cifPTmgVZPIj8AgPc%3D',
      },

      {
        id: 'snack3',
        name: '恰恰香瓜子',
        standard: '260g/包',
        productPrice: '8.5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8cNxnUYVWTAULkFh6IasAaFjVNucyuiTCfYVN2fGi_IBg%3D%3D.jpg?Expires=253362927570&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=J8kZ%2Fix%2F3nY1ycsOBTlBihGqZFI%3D',
      },

      {
        id: 'snack4',
        name: '正林香瓜子',
        standard: '120g/包',
        productPrice: '4.5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8dmkskF8KV4jcjYDA12LLqPsbTD1LS7Lus5SvZPYrN1fw%3D%3D.jpg?Expires=253362927570&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=aUxiRJOw3L4P7UVO7FCViZFnbfc%3D',
      },
    ],

    dailyList: [
      {
        id: 'daily0',
        name: '3A扑克牌',
        standard: '50g/副',
        productPrice: '4.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8eRG_NH4YUmHYMk3qStDaXulvdWbVGrTMBxtdTvP7pe0A%3D%3D.jpg?Expires=253362927846&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=U3yew0t7UpgCOK%2Ba1%2Bl0KWI9hlM%3D',
      },

      {
        id: 'daily1',
        name: 'EC洁面湿巾',
        standard: '10片/包',
        productPrice: '5.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8eLiiJY7hjpuodhRAl8A7avaeNuA7PXT6rdWVOs8eKOJA%3D%3D.jpg?Expires=253362927846&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=Oo5JjLNKm%2FqfMVeiTeDzf%2BYpxmE%3D',
      },

      {
        id: 'daily2',
        name: '南孚碱性5号电池',
        standard: '5粒/盒',
        productPrice: '16.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8c9MqSsV8iB10oh6qi7_LA_ZPmMMW87fqUA2AEtGo5eBg%3D%3D.jpg?Expires=253362927846&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=LoFZBkteHUh6tzTcO7QjYMVwoc8%3D',
      },

      {
        id: 'daily3',
        name: '汰渍净白去渍无磷洗衣粉',
        standard: '5kg/包',
        productPrice: '30.0',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8ctm6KHRQh25wq9fNevFd83ivXxr0oZNgYuDcNc_MHISQ%3D%3D.jpg?Expires=253362927847&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=%2FQuQNsm4jYUiwNDEyXDvXnLMOnI%3D',
      },

      {
        id: 'daily4',
        name: '立白超洁清新洗衣粉',
        standard: '4kg/包',
        productPrice: '30.5',
        amount: 0,
        src: 'https://smt-profile.oss-cn-shenzhen.aliyuncs.com/N2IUcxGWUUW6_KuN9CnsDQKhli1VdY7DfMjMgRFWT8cMuec8WzUi--jEcM774sxzLjfcspAmawha5VehcjxSdQ%3D%3D.jpg?Expires=253362927847&OSSAccessKeyId=LTAIgvOoCpQSQKzC&Signature=rxL8PVqOWMUhbr0hSkQqOMeBQ98%3D',
      },
    ]
  },

  openOrder: function () {
    wx.navigateTo({
      url: `/pages/order/order?carArray=` + JSON.stringify(this.data.carArray) + '&totalPrice=' + this.data.totalPrice,
    })
  },

  onShareAppMessage: function (res) {
    return {
      path: '/pages/index/index',
      title: '转发给好友',
    }
  },

  stopMove() {
  },

  kindToggle: function (e) {
    var id = e.currentTarget.id;
    var list = this.data.list;
    for (var i = 0, len = list.length; i < len; ++i) {
      if (list[i].id == id) {
        list[i].open = !list[i].open
      } else {
        list[i].open = false
      }
    }
    this.setData({
      list: list,
      curIndex: e.currentTarget.dataset.index,
      childIndex: 0,
    })
  },

  selected: function (f) {
    this.setData({
      childIndex: f.currentTarget.dataset.chindex,
    })
  },

  addAmount: function (g) {
    var kind = g.currentTarget.dataset.kind;
    const chooseList = this.data[kind];
    var index = g.currentTarget.dataset.prolistindex;
    var newAmount = kind + "[" + index + "].amount";
    var mark = kind + index; // 唯一的标识符
    var num = chooseList[index].amount;
    var name = chooseList[index].name;
    var price = chooseList[index].productPrice;
    var carArray1 = this.data.carArray;
    var totalAmount = this.data.totalNumber;
    num += num == 99 ? 0 : 1;
    totalAmount += totalAmount == 99 ? 0 : 1;
    var obj = { name: name, num: num, price: price, mark: mark, kind: kind, index: index };
    var arrIndex = carArray1.findIndex(item => item.mark == mark);
    if (arrIndex == -1) {
      carArray1.push(obj);
    } else {
      carArray1[arrIndex] = obj;
    }
    this.setData({
      carArray: carArray1,
      [newAmount]: num,
      totalNumber: totalAmount
    });
    this.calTotalPrice();
  },

  delAmount: function (g) {
    var kind = g.currentTarget.dataset.kind;
    var chooseList = this.data[kind];
    var index = g.currentTarget.dataset.prolistindex;
    var newAmount = kind + "[" + index + "].amount";
    var mark = kind + index; // 唯一的标识符
    var num = chooseList[index].amount;
    var name = chooseList[index].name;
    var price = chooseList[index].productPrice;
    var carArray1 = this.data.carArray;
    var totalAmount = this.data.totalNumber;
    totalAmount -= totalAmount == 0 ? 0 : 1;
    num -= num == 0 ? 0 : 1;
    var obj = { name: name, num: num, price: price, mark: mark, kind: kind, index: index };
    var arrIndex = carArray1.findIndex(item => item.mark == mark)
    carArray1[arrIndex] = obj
    carArray1 = carArray1.filter(item => item.num > 0) // 当某商品数量为0时，在购物车删除该商品
    if (carArray1.length == 0) {
      this.setData({
        list_opened:false
      })
    } // 当商品总数为0时，关闭购物车窗口
    this.setData({
      carArray: carArray1,
      [newAmount]: num,
      totalNumber: totalAmount
    });
    this.calTotalPrice();
  },

  // precise: function (x) {
  //   return Number.parseFloat(x).toPrecision(5);
  // },

  // 开关购物车list
  switchList: function () {
    this.setData({
      list_opened: !this.data.list_opened
    })
  },

  // 计算总价格
  calTotalPrice: function () {
    var len = this.data.carArray.length;
    var totalPrice = 0;
    for (var i = 0; i < len; ++i) {
      let item = this.data.carArray[i];
      totalPrice += item.num * item.price;
    }
    this.setData({
      totalPrice: totalPrice,
    })
  },

  // Counter: function () {
  //   var counter = this.data.counter + 1;
  //   this.setData({
  //     counter: counter
  //   })
  // },
})