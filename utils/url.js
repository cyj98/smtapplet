const domainName = "www.simianti.top";
const projectName = "smtProfile";
const app = getApp();

// 发送code
export const codeUrl = `https://${domainName}/${projectName}/identity/code`;

// 查询登录状态
export const loginstatusUrl = `https://${domainName}/${projectName}/identity/loginStatus`;

// 注册
export const registerUrl = `https://${domainName}/${projectName}/identity/register`;

// 登录
export const loginUrl = `https://${domainName}/${projectName}/identity/login`;

// 登出
export const logoutUrl = `https://${domainName}/${projectName}/identity/logout`;

// 从数据库获取图片信息
export const imageListUrl = `https://${domainName}/${projectName}/image/list`;

// 上传图片
export const uploadImageUrl = `https://${domainName}/${projectName}/image/upLoadImage`;

// 删除图片
export const deleteImageUrl = `https://${domainName}/${projectName}/image/deleteImage`;

export const getImageUrl = `https://${domainName}/${projectName}/image/url`;

export const checkVersionUrl = `https://${domainName}/${projectName}/update/checkVersion`;

export const updateTimeUrl = `https://${domainName}/${projectName}/update/updateTime`;

// export const /

// export const getImage = (name, callback) => {
//   return new Promise(resolve => {
//     wx.request({
//       url: getImageUrl + name,
//       method: 'POST',
//       header: app.globalData.header,
//       success(res) {
//         callback(res.data.resData);
//       },
//     })
//   })
// };

// 下面的地址配合云端 Server 工作

// 查询注册状态
// regStatusUrl: `https://${host}/${identity}/regStatus`,

// 登录地址，用于建立会话
// loginUrl: `https://${host}/${identity}/login`,

// 退出登录
// logoutUrl: `https://${host}/${identity}/logout`,

// 在服务器上注册用户
// registerUrl: `https://${host}/${identity}/register`,

// 测试的请求地址，用于测试会话
// requestUrl: `https://${host}/testRequest`,

// 测试的信道服务接口
// tunnelUrl: `https://${host}/tunnel`,

// 生成支付订单的接口
// paymentUrl: `https://${host}/payment`,

// 发送模板消息接口
// templateMessageUrl: `https://${host}/templateMessage`,

// 上传文件接口
// uploadFileUrl: `https://${host}/upload`,

// 下载示例图片接口
// downloadExampleUrl: `https://${host}/static/weapp.jpg`
// };

// export { codeUrl, loginstatusUrl, registerUrl, loginUrl, logoutUrl, imageListUrl, uploadImageUrl, deleteImageUrl}