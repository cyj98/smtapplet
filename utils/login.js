import { codeUrl, loginstatusUrl, registerUrl, loginUrl, logoutUrl } from './url.js'

const loginTimer = _ => {
  setInterval(_ => checkLogin(), 7200000)
}

// 其实只要
// 根据cookie判断是否登录，若未登录，则更新cookie
export const checkLogin = (resolve, reject) => {
  const app = getApp();
  wx.request({
    url: loginstatusUrl,
    header: app.globalData.header,
    success: res => {
      if (res.statusCode != 200) {
        wx.showModal({
          title: '登录失败',
          content: '服务器可能正在维护，请稍后再试',
          showCancel: false
        })
        reject()
        return;
      }
      console.log('第一步：查询登录状态成功，打印登录状态')
      const loginStatus = res.data.resData.loginStatus
      console.log("checkLogin",res)
      if (loginStatus == 'login') {
        console.log("用户已在第三方服务器登录")
        wx.checkSession({
          fail: function () {
            console.log('session过期，重新走登录流程');
            wxLogin(resolve, reject);
          },
          success: _ => {
            console.log("用户已在微信登录");
            wx.getUserInfo({
              lang: 'zh_CN',
              success: res => {
                const userInfo = JSON.parse(res.rawData)
                app.globalData.userInfo = userInfo
                resolve(userInfo)
              }
            })
            loginTimer();
          },
        })
      } else {
        const cookie = res.header['Set-Cookie']
        if (cookie) {
          console.log('更新请求头')
          app.globalData.header = {
            cookie
          }
          wxLogin(resolve, reject);
        } else {
          if (loginStatus == 'logout' || loginStatus == 'notLoggedIn') {
            console.log("用户已在服务器已登出或未登录，正在重新登录")
            wxLogin(resolve, reject);
          } else {
            // 这部分貌似没什么用
            console.log("获取cookie失败");
            wx.showModal({
              title: '登录失败',
              content: '登录出错或自动重新登录超时，请退出微信小程序重新登录',
              showCancel: false
            })
            reject();
          }
        }
      }
    }
  })
}

const wxLogin = (resolve, reject) => {
  const app = getApp();
  console.log('第二步：获取code')
  wx.login({//获取code
    success: function (res) {
      let code = res.code;
      // 这个code 其实通过检查错误码更好，不过
      if (code) {
        console.log('第三步：发送code到第三方服务器')
        wx.request({
          url: codeUrl + '?code=' + code,
          data: { code: code },
          header: app.globalData.header,
          method: "POST",
          success: function (res) { //发送成功
            if (res.statusCode != 200) {
              wx.showModal({
                title: '登录失败',
                content: '发送登录请求到服务器失败，请稍后再试',
                showCancel: false
              })
              reject();
              return;
            }
            console.log('wxLogin',res)
            const resData = res.data.resData;
            if (resData == "用户未注册过") {
              // 下面这个赋值其实没必要，历史遗留原因
              app.globalData.registerStat = false;
              if (app.globalData.grantInfo) {
                register(resolve, reject)
              }
            } else {
              app.globalData.registerStat = true;
              if (resData == '用户已注册过' || resData == '修改session_key成功') {
                  serverLogin(resolve, reject);
              } else {
                consofle.log("发送code后遇到错误");
                wx.showModal({
                  title: '登录失败',
                  content: '与第三方服务器连接失败',
                  showCancel: false
                })
                reject();
              }
            }
          },
          fail: function (res) {
            console.log("发送code失败")
            wx.showModal({
              title: '登录失败',
              content: '与第三方服务器连接失败，请稍后再试',
              showCancel: false
            })
            reject();
          },
        })
      } else {
        console.log('获取code失败')
        wx.showModal({
          title: '登录失败',
          content: '与微信服务器连接失败，请稍后再试',
          showCancel: false
        })
        reject();
      }
    }
  })
}

// 向服务器发送用户信息进行注册
export const register = (resolve, reject) => {
  const app = getApp();
  console.log('第四步，发送用户信息到服务器进行注册')
  wx.getUserInfo({
    lang: 'zh_CN',
    success: res => {
      const userInfo = JSON.parse(res.rawData)
      wx.request({
        url: registerUrl,
        method: "POST",
        header: app.globalData.header,
        data: JSON.stringify({
          nickname: userInfo.nickName,
          gender: userInfo.gender,
          city: userInfo.city,
          province: userInfo.province,
          country: userInfo.country,
          avatar_url: userInfo.avatarUrl
        }),
        success: function (res) { //注册成功，向服务器发送登录请求
          if (res.statusCode != 200) {
            wx.showModal({
              title: '登录失败',
              content: '注册失败，请稍后再试',
              showCancel: false
            })
            reject()
            return;
          }
          console.log('register', res)
          const resData = res.data.resData;
          if (resData == '用户信息注册成功') {
            console.log("注册成功")
            app.globalData.registerStat = true;
            serverLogin(resolve, reject)//在第三方服务器登录
          } else {
            wx.showModal({
              title: '注册失败',
              content: '向服务器发送注册请求出错，请退出后重新登录',
              showCancel: false
            })
            reject();
          }
        },
        fail: function (res) { //注册失败，提示用户退出小程序重新登录
          console.log(res)
          wx.showModal({
            title: '注册失败',
            content: '向服务器发送注册请求失败，请退出后重新登录',
            showCancel: false
          })
          reject();
        },
        complete: res => {
          console.log('register', res)
        }
      })
    }
  })
}

// 已成功，向服务器发送登录请求
const serverLogin = (resolve, reject) => {
  const app = getApp();
  console.log('第四/五步：在第三方服务器登录')
  wx.request({
    url: loginUrl,
    method: "POST",
    data: {
      // login_location: "登录地点",
      login_way: "通过微信登录"
    },
    header: app.globalData.header,
    success: function (res) { //登录成功，前端直接显示用户信息
      if (res.statusCode != 200) {
        wx.showModal({
          title: '登录失败',
          content: '服务器登录失败，请退出登陆后重试',
          showCancel: false
        })
        reject();
        return;
      }
      console.log("发送登录请求成功，打印返回的res")
      console.log(res)
      wx.getUserInfo({
        lang: 'zh_CN',
        success: res => {
          const userInfo = JSON.parse(res.rawData)
          app.globalData.userInfo = userInfo
          wx.setStorage({
            "key": 'userData',
            "data": JSON.stringify(app.globalData),
          })
          console.log(app.globalData);
          resolve(userInfo);
        }
      })
      loginTimer();
      // callback();
    },
    fail: function (res) { //登录失败，提示用户退出小程序重新登录
      console.log("登录失败,打印返回的res")
      console.log(res)
      wx.showModal({
        title: '登录失败',
        content: '登录出错或自动重新登录超时，请退出微信小程序重新登录',
        showCancel: false
      })
      reject();
    },
  })
}

export const logout = (resolve, reject) => {
  const app = getApp();
  app.globalData.userQuitting = true;
  wx.request({
    url: logoutUrl,
    method: "POST",
    header: app.globalData.header,
    success: function (res) {
      if (res.statusCode != 200) {
        wx.showModal({
          title: '退出失败',
          content: '你已退出登录或服务器可能正在维护，请稍后再试',
          showCancel: false
        })
        reject();
        return;
      }
      app.globalData.userInfo = null;
      app.globalData.userQuitting = false;
      app.globalData.loginPromise = Promise.reject()
      resolve();
    },
    fail: res => {
      app.globalData.userQuitting = false;
      reject();
    },
    complete: res => {
      console.log('logout\n',res);
    }
  })
} 