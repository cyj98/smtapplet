export const checkNetwork = (resolve = _ => { }, reject = _ => { }) => {
    wx.getNetworkType({
      complete: res => {
        if (res.networkType == 'none') {
          wx.showModal({
            title: '连接失败',
            content: '网络连接失败，请检查你的网络状态',
            showCancel: false
          })
          // return Promise.reject()
          reject()
        } else {
          // return Promise.resolve()
          resolve()
        }
      }
    })
}