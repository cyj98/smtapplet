export const sendSuccess = (msg, title = '操作失败', content = '操作失败，请稍后再试') => {
  if (msg != 200) {
    wx.showModal({
      title,
      content,
      showCancel: false
    })
    return false;
  } else {
    return true;
  }
}

export const dateCompare = (a, b) => {
  if (a > b) {
    wx.showModal({
      title: '信息错误',
      content: '入职时间晚于离职时间，请确认信息无误后再保存',
      showCancel: false
    })
    return false;
  } else {
    return true;
  }
}