const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

export const formatTime = date => {
  date = new Date(date);
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

export const formatMonth = date => {
  date = new Date(date);
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  return [year, month].map(formatNumber).join('-');
}

export const formatGraduate = date => {
  date = new Date(date);
  const year = date.getFullYear() + 4;
  const month = date.getMonth() + 1;
  return [year, month].map(formatNumber).join('-');
}

const formatHour = date => {
  date = new Date(date);
  const hour = date.getHours()
  const minute = date.getMinutes()
  return [hour, minute].map(formatNumber).join(':')
}

export const addHalfHour = date => {
  date = new Date(date);
  const minutesToAdjust = 30;
  const millisecondsPerMinute = 60000;
  let arrival_time = new Date(date.valueOf() + (minutesToAdjust * millisecondsPerMinute));
  return formatHour(arrival_time);
}

// export const formatTime = formatTime
// export const addHalfHour = addHalfHour
// export const formatMonth = formatMonth