export const checkInclude = function arrayContainsArray(superset, subset) {
  if (0 === subset.length) {
    return false;
  }
  return subset.every(function (value) {
    return (superset.indexOf(value) >= 0);
  });
}

export const checkComplete = (resume, nameList) => {
  let keys = Object.keys(resume);
  keys.forEach(key => {
    if (!resume[key] && resume[key] !== 0 && resume[key] !== '0') delete resume[key]
  })
  keys = Object.keys(resume);
  if (!checkInclude(keys, nameList)) {
    wx.showModal({
      title: '保存失败',
      content: '请在填写完整信息后保存',
      showCancel: false
    })
    return false;
  } else {
    return resume;
  }
} 

export const changeInput = (e, kind, that) => {
  const subKind = `${kind}.` + e.currentTarget.dataset.name
  that.setData({
    [subKind]: e.detail.value
  })
}

export const changePicker = (e, kind, that) => {
  const name = e.currentTarget.dataset.name;
  const index = e.detail.value;
  const subKind = `${kind}.` + name;
  const indexName = name + "Index";
  const arrayName = name + "Array";
  that.setData({
    [indexName]: index,
    [subKind]: that.data[arrayName][index]
  })
}

export const formatInfo = that => {
  // 格式化总简历里的数据，用来展示城市和年龄
  const info = that.data.info;
  let city = info.location.split(' ')[1];
  city = city.substring(0, city.length - 1);
  const birthYear = info.birth_date.split('-')[0];
  const now = new Date();
  const year = now.getFullYear();
  that.setData({
    city,
    age: year - birthYear,
  })
}